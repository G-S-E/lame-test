#! /bin/bash
# ./configure --prefix=/mingw  --enable-static
# make -j
# install or copy libmp3lame.a to ./bin/
# install or copy lame.h to ./include/
# 
# then build the lame-test. Remove -DDEBUG_BUILD for production build
gcc src/lame-test.c -Wall -Wextra -O3\
   -I./ -L./bin/ -lmp3lame.linux -pthread -lm -static \
   -o bin/lame-test 