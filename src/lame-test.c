//============================================================================
// Name        : lame-test.c
// Author      : Sergei Golubtsov
// Version     : 1.00
// Copyright   : GPLv3
// Description : Simple LAME front-end for wav to mp3 file conversion
//============================================================================

#ifndef CONV_CHUNK_SIZE
#define CONV_CHUNK_SIZE 96000 // number of samples for a convention cycle
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <stdarg.h>
#include <errno.h>
#include <pthread.h>
#include <include/lame.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#define WAV_CONV_BUFF_SIZE 2 * CONV_CHUNK_SIZE // WAV conversion buffer size
#define MP3_CONV_BUFF_SIZE 5 * CONV_CHUNK_SIZE / 4 + 7200 // MP3 conversion buffer size

/*! @brief thread parameters
 *  Passed to a thread while it's creation
 */
typedef struct thread_parameters {
	pthread_mutex_t mtx; // these parameters guard
	DIR *dp; // the working directory
	char *fn; // file name buffer pointer
	char *path; // path to convert
} thread_parameters;

static char *get_next_wav_file (DIR *dp, char *fn);
static size_t convert_wav_to_mp3 (lame_global_flags *lgfp, char *path, char *fn,
		short int *wvbp, size_t wvbs, unsigned char *mpbp, size_t mpbs);
static void *thread (void *tpars);

/*! @brief Entry point
 * Main conversion thread also starts new threads if needed and wait for them at the end.
 *
 */
int main (int argc, char **argv)
{
	// check the arguments
	if (argc != 2)
	{
		fprintf (stderr, "Wrong number of arguments passed.\n");
		fprintf (stderr, "Use a path contains *.wav files to convert as single argument:\n");
#ifdef _WIN32
		fprintf (stderr, "   lame-test.exe D:\\path\\to\\wav_files\\ \n");
#else
		fprintf (stderr, "   lame-test ./path/to/wav_files/ \n");
#endif
		exit (EXIT_FAILURE);
	}

	// open the directory containing wav files
	thread_parameters tpars;
	if ((tpars.dp = opendir (argv[1])) == NULL)
	{
		fprintf(stderr, "   ERROR %d while opendir (): %s: %s\n", errno, strerror(errno), argv[1]);
		exit (EXIT_FAILURE);
	}
#ifdef DEBUG_BUILD
	fprintf (stdout, "Directory %s was opened.\n", argv[1]);
#endif

	// copy path to thread parameters
	tpars.path = malloc (strlen (argv[1]) + 1);
	strcpy (tpars.path, argv[1]);

	// initialize the LAME library
	fprintf (stdout, "LAME library version used: %s\n", get_lame_version ());
	lame_global_flags *lgfp = lame_init ();
	lame_set_quality (lgfp, 5); // 5 - "good" as docs mentioned
	int r = 0; // result
	r = lame_init_params (lgfp);
	if (r)
	{
		fprintf(stderr, "   ERROR %d while lame_init_params ()\n", r);
		exit (EXIT_FAILURE);
	}

	// fill thread parameters
	pthread_mutex_init (&tpars.mtx, NULL);
	// get cores count of the system running
#ifdef _WIN32
	SYSTEM_INFO sysinfo;
	GetSystemInfo (&sysinfo);
	int mthrds = sysinfo.dwNumberOfProcessors;
#else
	int mthrds = sysconf (_SC_NPROCESSORS_ONLN);
#endif
	int nthrds = 0; // number of running additional threads
	pthread_t *t = malloc (mthrds * sizeof (pthread_t));
	memset (t, 0, mthrds * sizeof (pthread_t));
	tpars.fn = NULL;

	// file name buffers
	// we need to use them because of readdir () implementation for WIN :(
	char fn1[FILENAME_MAX] = ""; // file name buffer for conversion in this thread
	char fn2[FILENAME_MAX] = ""; // file name buffer for passing to a new thread

	// allocate path string buffer = path str len + max file name size + 1
	char *pbp = malloc (strlen (tpars.path) + sizeof (fn1) + 1);
	strcpy (pbp, tpars.path);
	// allocate wav samples buffer for all channels
	short int wvbp [WAV_CONV_BUFF_SIZE];
	// allocate mp3 samples buffer
	unsigned char mpbp [MP3_CONV_BUFF_SIZE];

	// fetch wav files, convert them or start new threads and pass the file to them
#ifdef DEBUG_BUILD
	fprintf (stdout, "Starting threads creation.\n");
#endif
	pthread_mutex_lock (&tpars.mtx);
	while (get_next_wav_file (tpars.dp, fn1))
	{
		// create threads if necessary and possible
		if ((tpars.fn == NULL) // previous thread have copied necessary parameters
			&& (nthrds < mthrds - 1) // we already run one thread, so "- 1" comes
				&& get_next_wav_file (tpars.dp, fn2))
		{
			tpars.fn = fn2; // pass founded file to the new thread
			pthread_mutex_unlock (&tpars.mtx);

			// create thread
#ifdef DEBUG_BUILD
			fprintf (stdout, "Trying to start thread N = %d\n", nthrds);
#endif
			if ((r = pthread_create (&t[nthrds], NULL, thread, (void*) &tpars)) == 0)
				nthrds ++;
			else fprintf (stderr, "   ERROR while thread creation. Return code: %d\n", r);
		}
		else pthread_mutex_unlock (&tpars.mtx);

		// convert wav file
		r = convert_wav_to_mp3 (lgfp, pbp, fn1, wvbp, WAV_CONV_BUFF_SIZE,
				mpbp, MP3_CONV_BUFF_SIZE);
		// if error occur while thread creation
		if (r) fprintf (stderr, "   ERROR %d occur while conversion of file %s\n", r, fn1);
		pthread_mutex_lock (&tpars.mtx);
	}

	pthread_mutex_unlock (&tpars.mtx);

	lame_close (lgfp);
	free (pbp); // path buffer isn't sheared with other threads

	// wait other threads to terminate
	void *trv = NULL; // exit status of target thread
	while (-- nthrds >= 0)
	{
		// pthread_join error
		r = pthread_join (t [nthrds], &trv);
		// error while pthread_join call
		if (r)
			{
			fprintf (stderr, "   ERROR while pthread_join () of thread num = %d, ID: %lx. Error number: %d\n",
					nthrds, (long unsigned int) t [nthrds], r);
			free (t);
			exit (r);
			}
		// target thread return error on pthread_exit
		if (trv)
			{
			fprintf (stderr, "   ERROR Thread ID: %lx returned on exit: %ld\n",
					(long unsigned int) t [nthrds], (long int) trv);
			free (t);
			exit ((long int) trv);
			}
#ifdef DEBUG_BUILD
		fprintf (stdout, "Thread ID: %lx joined to main thread\n", (long unsigned int) t [nthrds]);
#endif
		trv = NULL;
	}

	free (tpars.path);
	free (t);

	exit (EXIT_SUCCESS);
}

/*! @brief thread function
 *
 */
static void *thread (void *tpars)
{
#ifdef DEBUG_BUILD
	fprintf (stdout, "Thread created. ID: %lx\n", (long int) pthread_self ());
#endif
	// copy necessary thread parameters
	thread_parameters *tp = (thread_parameters*) tpars;
	pthread_mutex_lock (&tp->mtx);
	DIR *dp = tp->dp;
	// file name buffers
	// we need to use them because of readdir () implementation for WIN :(
	char fn[FILENAME_MAX]; // file name buffer for conversion in this thread
	strcpy (fn, tp->fn);
	tp->fn = NULL; // it indicates to main thread that necessary thread parameters were copied
	pthread_mutex_unlock (&tp->mtx);

	// setup LAME configuration
	lame_global_flags *lgfp = lame_init ();
	lame_set_quality (lgfp, 5); // 5 - "good" as docs mentioned
	long int r = 0; // result
	r = lame_init_params (lgfp);
	if (r)
	{
		fprintf(stderr, "   ERROR %ld while lame_init_params ()\n", r);
#ifdef DEBUG_BUILD
		fprintf (stdout, "Thread terminated. ID: %lx\n", (long int) pthread_self ());
#endif
		pthread_exit ((void*) r);
	}

	// allocate path string buffer = path str len + max file name size + 1
	char *pbp = malloc (strlen (tp->path) + sizeof (fn) + 1);
	strcpy (pbp, tp->path);
	// allocate wav samples buffer for all channels
	short int wvbp [WAV_CONV_BUFF_SIZE];
	// allocate mp3 samples buffer
	unsigned char mpbp [MP3_CONV_BUFF_SIZE];

	// read wav files pointers
	pthread_mutex_lock (&tp->mtx);
	do
	{
		pthread_mutex_unlock (&tp->mtx);
		// convert wav file
		r = convert_wav_to_mp3 (lgfp, pbp, fn, wvbp, WAV_CONV_BUFF_SIZE,
				mpbp, MP3_CONV_BUFF_SIZE);
		// if error occur while thread creation
		if (r) fprintf (stderr, "   ERROR %ld occur while conversion of file %s\n",
						r, fn);

		pthread_mutex_lock (&tp->mtx);
	// get next wav file
	} while (get_next_wav_file (dp, fn) != NULL);

	pthread_mutex_unlock (&tp->mtx);

	lame_close (lgfp);
	free (pbp);

#ifdef DEBUG_BUILD
	fprintf (stdout, "Thread terminated. ID: %lx\n", (long int) pthread_self ());
#endif
	pthread_exit (EXIT_SUCCESS);
	return NULL; // Suppress  warning: no return statement in function returning non-void [-Wreturn-type]
}

/*! @brief wav file fetching
 *         Iterate over the directory and search for wav files
 *
 *  @param[in] 		dp		directory to search
 *  @param[in/out] 	fn   	buffer to copy wav file name founded
 *
 *  @return  		NULL 	if there are no more wav files in the directory
 *  				fn		file name buffer pointer if file founded
 */
static char *get_next_wav_file (DIR *dp, char *fn)
{
#ifdef DEBUG_BUILD
	fprintf (stdout, "Trying to find a wav file. Thread ID: %lx\n", (long int) pthread_self ());
#endif

	// get next entry while end of the dir
	errno = 0;
	struct dirent *ep;
	char *fnbp = fn;
	fn = NULL;
	while ((ep = readdir (dp)) != NULL)
	{
		// check whether file is .wav or not
		if (strlen (ep->d_name) < 4
				|| strcmp (ep->d_name + strlen (ep->d_name) - 4, ".wav"))
			continue;

		// if wav file found
		if (strcmp (ep->d_name + strlen (ep->d_name) - 4, ".wav") == 0)
		{
#ifdef DEBUG_BUILD
			fprintf (stdout, " Thread  ID: %lx. WAV file found: %s\n",
					(long int) pthread_self (), ep->d_name);
#endif
			// copy file name to buffer because of the readdir () WIN implementation
			strcpy (fnbp, ep->d_name);
			fn = fnbp;
			break;
		}
	}

	// an error is detected or no more entries in the directory
	if ((ep == NULL) && errno)
		fprintf (stderr, "   ERROR %d: %s\n", errno, strerror(errno));

	return fn;
}

/*! @brief wav file conversion
 *         Convert the wav file to mp3
 *
 *  @param[in] 		lgfp	LAME parameters (conversion quality, etc.)
 *  @param[in] 		path	pointer to directory of operation path name
 *  @param[in]	 	fn   	pointer to wav file name
 *  @param[in]		wvbp	pointer to wav samples buffer
 *  @param[in]		wvbs	size of wav samples buffer (bytes)
 *  @param[in]		mpbp	pointer to mp3 data buffer
 *  @param[in]		mpbs	size of mp3 data buffer (bytes)
 *
 *  @return  		0 		conversion was successfully done
 *  				!=0		error code
 *  				EXIT_FAILURE error while file operation
 */
static size_t convert_wav_to_mp3 (lame_global_flags *lgfp, char *path, char *fn,
		short int *wvbp, size_t wvbs, unsigned char *mpbp, size_t mpbs)
{
#ifdef DEBUG_BUILD
	fprintf (stdout, "Conversion started. Thread  ID: %lx File: %s\n",
			(long int) pthread_self (), fn);
#endif
	// construct path for wav file
	strcat (path, fn);
	// open wav file
	FILE *wvfp;
	if ((wvfp = fopen (path, "rb")) == NULL)
	{
		fprintf (stderr, "   ERROR while opening wav file %s. Thread  ID: %lx\n",
				path, (long int) pthread_self ());
		return EXIT_FAILURE;
	}

	// construct path for mp3 file
	strcpy (path + strlen (path) - 4, ".mp3");
	// open mp3 file
	FILE *mpfp;
	if ((mpfp = fopen (path, "w+b")) == NULL)
	{
		fprintf (stderr, "   ERROR while opening mp3 file %s\n", path );
		fclose (wvfp);
		path[strlen (path) - strlen (fn)] = 0; // rempve file name from path
		return EXIT_FAILURE;
	}
	path[strlen (path) - strlen (fn)] = 0; // rempve file name from path

	// convert wav to mp3
	int nbr; // bytes were readed
	int nbw; // bytes to write
	size_t r; // result
	while ((nbr = fread (wvbp, 2 * sizeof (short int), wvbs / 2, wvfp)) > 0)
	{
		// do wav to mp3 conversion
		nbw = lame_encode_buffer_interleaved (lgfp, wvbp, nbr / 2, mpbp, mpbs);
#ifdef DEBUG_BUILD
		fprintf (stdout, "LAME has converted %d bytes of mp3\n", nbw);
#endif
		// error while lame conversion
		if (nbw < 0)
		{
			fprintf (stderr, "   ERROR %d occurs while lame_encode_buffer_interleaved\n", nbw);
			break;
		}

		// write chunk to mp3 file
#ifdef DEBUG_BUILD
		fprintf (stdout, "Writing %d bytes to file: %s%s.mp3\n", nbw, path, fn);
#endif
		if ((r = fwrite (mpbp, nbw, 1, mpfp)) != 1)
		{
			fprintf (stderr, "   ERROR while fwrite () to mp3 file %s\n", path );
			path[strlen (path) - strlen (fn)] = 0; // rempve file name from path
			fclose (wvfp);
			fclose (mpfp);
			return r;
		}
	}

	// flush internal LAME buffers
	if (nbr == 0)
	{
		nbw = lame_encode_flush (lgfp, mpbp, mpbs);
#ifdef DEBUG_BUILD
		fprintf (stdout, "LAME converts %d bytes to mp3 while lame_encode_flush\n", nbw);
#endif
		// error while internal LAME buffers flush
		if (nbw < 0) fprintf (stderr, "   ERROR %d while lame_encode_buffer_interleaved\n", nbw);
		else
		{
			// write chunk to mp3 file
#ifdef DEBUG_BUILD
			fprintf (stdout, "Writing %d bytes to file: %s%s.mp3\n", nbw, path, fn);
#endif
			if ((r = fwrite (mpbp, nbw, 1, mpfp)) != 1)
			{
				fprintf (stderr, "   ERROR while fwrite () to mp3 file %s\n", path );
				path[strlen (path) - strlen (fn)] = 0; // rempve file name from path
				fclose (wvfp);
				fclose (mpfp);
				return r;
			}
			lame_mp3_tags_fid (lgfp, mpfp);
		}
	}

#ifdef DEBUG_BUILD
	fprintf (stdout, "Closing wav and mp3 files\n");
#endif
	fclose (wvfp);
	fclose (mpfp);

#ifdef DEBUG_BUILD
	fprintf (stdout, "Conversion ended. File: %s\n", fn);
#endif
	return EXIT_SUCCESS;
}
