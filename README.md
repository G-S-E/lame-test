Name        : lame-test.c
Author      : Sergei Golubtsov
Version     : 1.00
Copyright   : GPLv3
LAME library version used: 3.100.1 SVN revision - trunk / 6438

For building on a Windows host consult with build_c_win.sh

For building on a Linux host consult with build_win.sh
